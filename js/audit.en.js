
/**
 *  Confirmation window for links that would interrupt the auditing process
 *
 *  @param scope 
 *    A jQuery expression
 */
function intercept_links(scope) {
  // Intercept non-process links on the page
  $('a', scope)
    .filter(function() {
      return $(this).parents('.a-list').length < 1;
    })
    .click(function() {
      if (audit.node_state == 'in-process') {
        tag_node('unprocessed', audit.node_id, 'in-process');
      }
      audit.exit_url = $(this).attr('href');

      $('body')
        .append('<div id="a-shade-window">&nbsp;</div>')
        .find('div:last')
        .animate({opacity: 0.5}, 1)
        .height($('body').height() + "px")
        .end()
        .append('<div id="a-confirm" class="messages"><h2>Stop auditing?</h2>'+
'<div class="description">Unsaved changes will be lost.</div>'+
'<a id="a-yes" href="' + audit.exit_url + '">Yes, stop auditing</a>'+
'<a id="a-other" target="audit_window" href="' + audit.exit_url + '">No, open link in new window</a>'+
'<a id="a-no" href="' + audit.audit_url + '">No, return to auditing</a>'+
'</div></div>')
        .find('#a-no, #a-other')
        .one('click', function() {
          if (audit.node_state != 'processed') {
            tag_node('in-process', audit.node_id, 'unprocessed');
          }
          $('#a-shade-window, #a-confirm')
            .remove();
          if ($(this).attr('id') != 'a-other') {
            return false;
          }
        });
      return false;
    });
}

/**
 * Refreshes one of the lists,
 *   adds the [x] hover behavior,
 *   and intercepts the node links
 *
 * @param type
 *  One of {unprocessed, in-process, processed, bookmarked}
 */
function load_list(type) {
  $('#a-' + type)
    // Show a message the first time
    .filter(function() {
      return $(this).html().length < 2;
    })
    .html(make_message('Loading ...'))
    .end()

    // After the first time, fade instead
    .fadeTo('fast', 0.2)
    .load(audit.audit_url + "/list/" + type, {}, function() { 
      $("#a-" + type)
        .fadeTo('fast', 1)
        .find('li a')

        // [x] links
        .filter(function() {
          return type != 'unprocessed';
        })
        .parent('li')
        .hover(function() {
          // Uses a CSS positioning hack to set offsetParent
          $(this)
            .prepend('<div id="a-remove">[<a href="#">X</a>]</div>')
            .find('#a-remove')
            .css({left: (this.offsetLeft-$('#a-remove').width()), top: this.offsetTop})
            .find('a')
            .click(a_hover);
        }, function() {
          $(this)
            .find('#a-remove')
            .find('a')
            .unbind('click')
            .end()
            .remove();
        })
        .end() // parent
        .end() // filter out unprocessed

        // Links in the side lists load nodes into the form
        .click(function() {
          if ($(this).attr('nid') != audit.node_id) {
            if (audit.node_state == 'in-process') {
              tag_node('unprocessed', audit.node_id);
            }
            load_form($(this).attr('nid'), function () { load_list(type); });
          }
          return false;
        });

      // Activate links for the node we are editing
      $('.a-list a')
        .removeClass('active')
        .filter('[@nid=' + audit.node_id + ']')
        .addClass('active');
    });
}

/**
 * Loads the node-edit form
 *
 * @param nid
 * @param fn (Optional)
 */
function load_form(nid, fn) {
  var nid = nid || '';
  if (nid) {
    nid = "/" + nid;
  }

  before_submit();
  $("#a-form")
    .load(audit.audit_url + "/form" + nid, {}, function() { 
      after_response();
      if ($.isFunction(fn)) {
        fn.apply();
      }
    });
}

/**
 * Changes the state of a node
 *
 * @param tag
 * @param nid
 * @param old_tag (Optional)
 */
function tag_node(tag, nid, old_tag) {
  if (tag == 'unprocessed' || tag == 'in-process' || tag == 'processed' || tag == 'bookmarked') {
    get_tag = reload_tag = tag;
  }
  else {
    get_tag = 'unbookmarked';
    reload_tag = 'bookmarked';
  }
  $.get(audit.audit_url + "/tag/" + nid + "/" + get_tag, function() {
    load_list(reload_tag);
    if (old_tag) {
      load_list(old_tag);
    }

    if (nid == audit.node_id && tag != 'bookmarked') {
      audit.node_state = tag;
    }
  });
}

/**
 * Used when the node-edit form or a list is reloading and has no content
 *
 * @param message
 * @param type (Optional: default = status)
 */
function make_message(message, type) {
  var type = type || 'status';
  return '<div class="a-messages messages ' + type + '">' + message + '</div>';
}

/**
 * Called by ajaxForm().
 * - Prepare the Submit button
 * - Scroll to the top
 */
function before_submit(formData, jqForm, options) {
  if (formData) {
    $.map(formData, function(i) {
      // Drupal requires a particular button label.
      if (i.name == 'op' && i.value == 'Submit changes') {
        i.value = 'Submit';
      }
      return i;
    });
  }

  // If we are at the bottom, we will miss the activity at the top.
  $('body')
    .ScrollTo('slow');
  $('#a-form')
    .html(make_message('Loading ...'));
}

/**
 * Called by ajaxForm().
 * - Set audit.node_id
 * - Reload the Drupal scripts
 * - Intercept links in the form
 * - (Re)load some lists
 * - Adjust some form elements
 * - Add process controls
 */
function after_response() {
  audit.node_id = $('#node-form input[@name=node_id]').val();

  // Load standard Drupal scripts so they may interact with the new form.
  $.each( ['drupal.js', 'autocomplete.js', 'collapse.js', 'textarea.js'], function (i, n) {
    // TODO: Reduce data transfered by calling functions rather than loading scripts.
    $.getScript(audit.base_url + '/misc/' + n);
  });

  intercept_links('#a-form');

  tag_node('in-process', audit.node_id);
  load_list('unprocessed');
  load_list('processed');
  
  // If the adjacent lists are empty, load them now.
  $('.a-list')
    .each(function() {
      if ($(this).html().length < 1) {
        load_list($(this).attr('type'));
      }
    });

  // Clone the Submit and Preview buttons to the top.
  $('#node-form')
    .find('input[@type=submit]')
    .filter('[@value=Submit]')
    .attr('value', 'Submit changes')
    .end() // Submit
    .filter('[@value^=Submit], [@value^=Preview]')
    .clone()
    .insertAfter($('label').filter('[@for=edit-title]').parent());

  // Attach the form handler to the new form.  Fix some attributes and elements.
  $('#node-form')
    .attr('action', audit.audit_url + "/form/" + audit.node_id)
    .ajaxForm({target: '#a-form', success: after_response, beforeSubmit: before_submit})
    .find('#edit-delete')
    .remove()
    .end()
    .find('option')
    .contains(audit.audit_id)
    .contains('in-process')
    .attr('selected', 'selected');

  // Move the textarea for log messages to the top.
  $("#node-form .form-item > label")
    .filter("[@for=edit-log]")
    .parent()
    .insertAfter($('label').filter('[@for=edit-title]').parent());

  // Fix previews to fit in the column
  $('#a-form')
    .find('.preview')
    .find('div')
    .removeClass('clear clear-block')
    .end() // div
    .end() // .preview
    .find('a-messages')
    .remove()
    .end()
    // Add process controls
    .prepend(audit_controls());

  // Add behaviors to the process controls
  $('#a-controls')
    .ajaxForm({target: '#a-form', success: after_response, beforeSubmit: before_submit})

    .find('input[@name=bookmarked]')
    .click(function() {
      tag_node('bookmarked', audit.node_id);
      return false;
    })
    .end()

    .find('input[@name=processed]')
    .click(function() {
      tag_node('processed', audit.node_id, 'in-process');
      return false;
    })
    .end()

    .find('input[@name=next]')
    .click(function() {
      if (audit.node_state == 'in-process') {
        tag_node('unprocessed', audit.node_id, 'in-process');
      }
    });
}

/**
 * Process control form
 */
function audit_controls() {
  return '<form action="' + audit.audit_url + '/form" method="POST" id="a-controls">' +
'<input type="submit" name="processed" class="form-submit" value="Mark as done" />' +
'<input type="submit" name="bookmarked" class="form-submit" value="Save for later" />' + 
'<input type="submit" name="next" class="form-submit" value="Skip to next" /></form>';
}

/**
 * Click handler for [x]
 */
function a_hover() {
  var a_type = $(this).parent().siblings('a').attr('type');
  var a_nid  = $(this).parent().siblings('a').attr('nid');
  if (a_type == 'processed' || a_type == 'in-process') {
    tag_node('unprocessed', a_nid, a_type);
  }
  else {
    tag_node('unbookmarked', a_nid, a_type);
  }
  return false;
}
