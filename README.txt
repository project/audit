Module: audit.module
File:   README.txt
Author: Nic Ivy (http://drupal.org/user/6194)
CVS:    $Id$

-----------------------------------------------------------------------------
INSTALLING

  1. Replace misc/jquery.js with jQuery 1.1.1 and make Drupal's JavaScript 
     files compatible with it.  (Use the included replacements at your own 
     risk.)
  2. Get nodequeue.module and install it.
  3. Get views.module and install it.
  4. Install this module in the normal way.
  5. Go to "administer" > "user management" > "access control" to configure
     user permissions.
  6. Create an audit process, as described below.

-----------------------------------------------------------------------------
CREATE AN AUDIT PROCESS

  1. Create a new audit process by going to "create content" > "audit
     process".  Make note of the node id, which we will call (id).

  2. Create three node queues. Their names must be like these:
       "Audit #(id): In-process"
       "Audit #(id): Done"
       "Audit #(id): Saved for later"
     You will get reminders if they do not exist.

  3. Create a view that lists unprocessed content.  The view must filter out
     the queues you just made.  Also, the view must be named like this:
       "audit_(id)_unprocessed"

-----------------------------------------------------------------------------
REQUIRED

  *  nodequeue module (Not included. http://drupal.org/project/nodequeue)
     The audit system depends on three queues for each audit process.

  *  views module (Not included. http://drupal.org/project/views)
     The audit system depends on a user-created view for each audit process.

  *  jQuery 1.1.1 (Not included. http://jquery.com/src/jquery-1.1.1.pack.js)
     Drupal 5.1 ships with jQuery 1.0.2, which is not compatible.  Upgrading
     may cause problems with collapse.js, textarea.js, and others.  See
     http://drupal.org/project/issues/drupal for suggested workarounds.

  *  The scroll-related methods in the Interface plugin for jQuery (Included)

  *  The jQuery Form module (Included)

  *  The jQuery dimensions plugin (Included)

-----------------------------------------------------------------------------
LOGS FOR RECORD KEEPING

  * User actions are recorded.  Go to "administer" > "logs" > "recent log
    entries" to view the records.  You can filter for audit-type events or go
    directly to http://example.com/admin/logs/watchdog/audit.

  * The records will be discarded, unless you save them.  Adjust your system
    settings to keep the records longer.  Go to "administer" > "site
    configuration" > "error reporting".

-----------------------------------------------------------------------------
UNINSTALLING

  1. In Drupal, go to "administer" > "site building" > "modules" to disable
     this module.
  2. After disabling, select the "unistall" tab at the top of the same page.
  3. Uninstall this module, which will delete all vocabularies and content 
     used by this module.  Logs will not be removed.

-----------------------------------------------------------------------------
LOCALE

  * The file js/audit.en.js contains static localized strings.

-----------------------------------------------------------------------------
KNOWN BUGS

  1. File attachments do not work in the auditing screen because of JavaScript
     entaglements.

-----------------------------------------------------------------------------
SUPPORT

  * http://drupal.org/project/audit

-----------------------------------------------------------------------------

